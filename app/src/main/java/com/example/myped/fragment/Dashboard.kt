package com.example.myped.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.myped.R
import com.example.myped.databinding.FragmentDashboardBinding

class Dashboard : Fragment() {
    private lateinit var binding: FragmentDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        val dashboardBinding = FragmentDashboardBinding.inflate(inflater, container, false)
        binding = dashboardBinding

        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.HomeNavigation) as NavHostFragment
        val navController = navHostFragment.navController
        val bottomNav = binding.bottomNavigation
        bottomNav.setupWithNavController(navController)
        return binding.root
    }
}