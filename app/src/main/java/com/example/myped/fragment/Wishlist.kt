package com.example.myped.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myped.databinding.FragmentWishlistBinding

class Wishlist : Fragment() {
        private lateinit var binding: FragmentWishlistBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val wishlistBinding = FragmentWishlistBinding.inflate(inflater, container, false)
        binding = wishlistBinding
        return binding.root
    }
}