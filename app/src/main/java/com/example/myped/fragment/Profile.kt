package com.example.myped.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.example.myped.R
import com.example.myped.companion.Snk
import com.example.myped.databinding.FragmentProfileBinding
import com.google.android.material.snackbar.Snackbar
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class Profile : Fragment() {
    private lateinit var binding: FragmentProfileBinding
    private lateinit var imageView: ImageView

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val selectedImageUri = data?.data

                selectedImageUri?.let { uri ->
                    loadImage(uri)
                }
            }
        }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // Handle the captured image from the camera
                val capturedImageBitmap = result.data?.extras?.get("data") as? Bitmap

                // Save the bitmap to a file and get the URI
                val capturedImageUri = saveBitmapToMediaStore(capturedImageBitmap)

                capturedImageUri?.let { uri ->
                    loadImage(uri)
                }
            }
        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val profileBinding = FragmentProfileBinding.inflate(inflater, container, false)
        binding = profileBinding
        imageView = binding.pp

        // Open gallery when ImageView is clicked
        imageView.setOnClickListener {
            showImageSourceDialog()
        }
        textChanger()
        binding.newusr.setOnClickListener {
            if (binding.userName.editText?.text.isNullOrBlank()){
                Snackbar.make(requireActivity().findViewById(android.R.id.content), "Name cannot be empty", Snackbar.LENGTH_SHORT).show()

            } else
            {                findNavController().navigate(R.id.action_profile_to_dashboard2)
            }
        }
        return profileBinding.root


    }

    private fun textChanger() {
        val sk = binding.snk  // Replace with your actual TextView ID
        sk.text = Snk.applyCustomTextextChanger(
            requireContext(),
            "Dengan masuk disini, kamu menyetujui Syarat & Ketentuan \n serta Kebijakan Privasi TokoPhincon."
        )
    }

    private fun showImageSourceDialog() {
        val options = arrayOf("Camera", "Gallery")

        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Choose Image Source")
            .setItems(options) { _, which ->
                when (which) {
                    0 -> openCamera()
                    1 -> openGallery()
                }
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

        builder.create().show()
    }

    private fun openGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryLauncher.launch(galleryIntent)
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraLauncher.launch(cameraIntent)
    }

    private fun loadImage(imageUri: Uri) {
        Glide.with(this)
            .load(imageUri)
            .apply(RequestOptions.bitmapTransform(CircleCrop()))
            .into(imageView)
    }

    private fun saveBitmapToMediaStore(bitmap: Bitmap?): Uri? {
        if (bitmap == null) return null

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val fileName = "IMG_$timeStamp.jpg"

        val resolver: ContentResolver = requireContext().contentResolver
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
        }

        val imageUri: Uri? =
            resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

        imageUri?.let {
            resolver.openOutputStream(it)?.use { outputStream: OutputStream ->
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            }
        }

        return imageUri
    }
}