package com.example.myped.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.myped.databinding.FragmentStoreBinding

class Store : Fragment() {
    private lateinit var binding: FragmentStoreBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val storeBinding = FragmentStoreBinding.inflate(inflater, container, false)
        binding = storeBinding
        checker(200)

        return storeBinding.root
    }
// cBack always 200
    private fun checker(cBack:Int){
    when (cBack) {
        200 -> {
            Toast.makeText(requireContext(),"HEHE",Toast.LENGTH_SHORT).show()
        }
        500 -> {
            Toast.makeText(requireContext(),"Internal Error",Toast.LENGTH_SHORT).show()
        }
        else -> {
            Toast.makeText(requireContext(),"Your connection is unavaliable ",Toast.LENGTH_SHORT).show()
        }
    }
    }
}