package com.example.myped.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myped.databinding.FragmentTransactionBinding

class Transaction : Fragment() {
    private lateinit var binding: FragmentTransactionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val transaction = FragmentTransactionBinding.inflate(inflater, container, false)
        binding = transaction
        return binding.root
    }
}