package com.example.myped.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.fragment.findNavController
import com.example.myped.R
import com.example.myped.databinding.FragmentHomeBinding
import com.example.myped.helper.Helper


class Home : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = homeBinding
        val themeChecker = Helper.getThemeStatus(requireContext(), "dark")
        binding.logout.setOnClickListener {
            val otherNavController =
                requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment)
                    ?.findNavController()
            otherNavController?.navigate(R.id.action_dashboard2_to_login)
        }
        if (themeChecker) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            binding.materialSwitch2.isChecked = true // Set switch state to checked
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            binding.materialSwitch2.isChecked = false // Set switch state to unchecked
        }
        doubleBackToExit()

        return homeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.materialSwitch2.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                true -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    Helper.putThemeStatus(requireContext(), "dark", true)
                }

                false -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    Helper.putThemeStatus(requireContext(), "dark", false)
                }
            }
        }
    }

    private fun doubleBackToExit() {
        var doubleBackPressed: Long = 0
        val toast =
            Toast.makeText(requireContext(), "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT)
        requireActivity().onBackPressedDispatcher.addCallback(this@Home) {
            if (doubleBackPressed + 2000 > System.currentTimeMillis()) {
                activity?.finish()
                toast.cancel()
            } else {
                toast.show()
            }
            doubleBackPressed = System.currentTimeMillis()
        }
    }
}
