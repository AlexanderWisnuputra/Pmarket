package com.example.myped.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.myped.R
import com.example.myped.companion.Snk
import com.example.myped.databinding.FragmentLoginBinding

class Login : Fragment() {
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val loginBinding = FragmentLoginBinding.inflate(inflater, container, false)
        binding = loginBinding
        binding.btnReg.setOnClickListener {profile()}
        binding.button.setOnClickListener {register()}
        textChanger()

        return loginBinding.root

    }
    private fun textChanger() {
        val sk =binding.textView5  // Replace with your actual TextView ID
        sk.text = Snk.applyCustomTextextChanger(
            requireContext(),
            "Dengan masuk disini, kamu menyetujui Syarat & Ketentuan \n serta Kebijakan Privasi TokoPhincon."
        )
    }
    private fun profile(){
        findNavController().navigate(R.id.action_login_to_profile)
    }
    private fun register(){
        findNavController().navigate(R.id.action_login_to_register)
    }
}




