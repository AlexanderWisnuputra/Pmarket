package com.example.myped.companion

import android.content.Context
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.core.content.ContextCompat
import com.example.myped.R

class Snk {

    companion object {

        fun applyCustomTextextChanger(context: Context, fullText: String): SpannableString {
            val spannableString = SpannableString(fullText)

            val startGreen = fullText.indexOf("Syarat & Ketentuan")
            val endGreen = startGreen + "Syarat & Ketentuan".length

            val startBlue = fullText.indexOf("Kebijakan Privasi")
            val endBlue = startBlue + "Kebijakan Privasi".length

            val customGreenColor = ContextCompat.getColor(context, R.color.primary)
            spannableString.setSpan(
                ForegroundColorSpan(customGreenColor),
                startGreen,
                endGreen,
                0
            )
            spannableString.setSpan(
                ForegroundColorSpan(customGreenColor),
                startBlue,
                endBlue,
                0
            )
            return spannableString
        }
    }
}

