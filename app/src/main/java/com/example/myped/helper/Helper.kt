package com.example.myped.helper

import android.content.Context
import android.content.SharedPreferences

class Helper {
    companion object {
        private const val SHARED_PREF_FILE = "userPreference"

        private fun getSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE)
        }

        fun putObStatus(context: Context, key: String, value: Boolean) {
            getSharedPreferences(context).edit().putBoolean(key, value).apply()
        }

        fun getObStatus(context: Context, key: String): Boolean {
            return getSharedPreferences(context).getBoolean(key, false)
        }
        fun putThemeStatus(context: Context, key: String, value: Boolean) {
            getSharedPreferences(context).edit().putBoolean(key, value).apply()
        }

        fun getThemeStatus(context: Context, key: String): Boolean {
            return getSharedPreferences(context).getBoolean(key, false)
        }
    }
}